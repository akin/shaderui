﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShaderUI
{
    public class NodeShard : ICloneable
    {
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; } = "";
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; } = "";
        [JsonProperty(PropertyName = "script")]
        public string Script { get; set; } = "";

        public object Clone()
        {
            var item = (NodeShard)MemberwiseClone();
            item.Language = (string)Language.Clone();
            item.Description = (string)Description.Clone();
            item.Script = (string)Script.Clone();
            return item;
        }
        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            var other = (NodeShard)obj;

            if (Language != other.Language || Description != other.Description || Script != other.Script)
            {
                return false;
            }
            return true;
        }
    }
}
