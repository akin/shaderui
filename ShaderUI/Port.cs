﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShaderUI
{
    public class Port
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; } = "";
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = "";
        [JsonProperty(PropertyName = "binding")]
        public string Binding { get; set; } = "";

        public object Clone()
        {
            var item = (Port)MemberwiseClone();
            item.Name = (string)Name.Clone();
            item.Type = (string)Type.Clone();
            item.Binding = (string)Binding.Clone();
            return item;
        }
        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            var other = (Port)obj;

            if (Name != other.Name || Type != other.Type || Binding != other.Binding)
            {
                return false;
            }
            return true;
        }
    }
}
