﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShaderUI.UI
{
    /// <summary>
    /// Interaction logic for NodeEditor.xaml
    /// </summary>
    public partial class NodeEditor : Window
    {
        private Node original;
        private Node edit;

        public NodeEditor(Node node)
        {
            InitializeComponent();
            Setup(node);
        }
        public void Setup(Node node)
        {
            original = node;
            edit = (Node)original.Clone();

            EditToUI();
        }
        private void UIToEdit()
        {
            edit.Name = nameInput.Text;
            edit.Type = typeInput.Text;
            edit.Description = descriptionInput.Text;
        }
        private void EditToUI()
        {
            nameInput.Text = edit.Name;
            typeInput.Text = edit.Type;
            descriptionInput.Text = edit.Description;
            connection.ItemsSource = null;
            connection.ItemsSource = edit.Connection;
        }
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            UIToEdit();
            if(edit.Equals(original))
            {
                this.Close();
            }
            original.Set(edit);
            if(original.OnUpdate != null)
            {
                original.OnUpdate(original);
            }
            this.Close();
        }
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void addShard_Click(object sender, RoutedEventArgs e)
        {
        }
        public static T? FindParentOfType<T>(DependencyObject obj)
        where T : DependencyObject
        {
            while (obj != null)
            {
                if (obj is T)
                {
                    return obj as T;
                }
                obj = VisualTreeHelper.GetParent(obj);
            }
            return null;
        }
        private void MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                var result = VisualTreeHelper.HitTest(connection, e.GetPosition(connection));
                DataGridRow? row = FindParentOfType<DataGridRow>(result.VisualHit);

                ContextMenu? cm = null;
                if (row == null)
                {
                    return;
                }
                cm = FindResource("contextMenu") as ContextMenu;
                cm.PlacementTarget = row;
                cm.DataContext = row;

                cm.IsOpen = true;
                e.Handled = true;
            }
        }
        public void RequestDelete(object sender, RoutedEventArgs args)
        {
            MenuItem? menuItem = sender as MenuItem;
            if (menuItem == null)
            {
                return;
            }
            DataGridRow? row = menuItem.DataContext as DataGridRow;
            if (row == null)
            {
                return;
            }

            Port port = row.DataContext as Port;
            if (port == null)
            {
                return;
            }
            edit.Connection.Remove(port);
            connection.ItemsSource = null;
            connection.ItemsSource = edit.Connection;
        }
    }
}
