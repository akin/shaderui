﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShaderUI.UI
{
    /// <summary>
    /// Interaction logic for ShaderEditor.xaml
    /// </summary>
    public partial class ShaderEditor : UserControl
    {
        private Shader Item = null;
        public string Name
        {
            get
            {
                return name.Text;
            }
            set
            {
                name.Text = value;
            }
        }
        public string Description
        {
            get
            {
                return description.Text;
            }
            set
            {
                description.Text = value;
            }
        }
        public ShaderEditor()
        {
            InitializeComponent();
        }
        public void Set(Shader item)
        {
            Item = item;
            ItemToUI();
        }
        public Shader Get()
        {
            return Item;
        }
        public void UIToItem()
        {
            Item.Name = name.Text;
            Item.Description = description.Text;
        }
        public void ItemToUI()
        {
            name.Text = Item.Name;
            description.Text = Item.Description;
        }
        private void save_Click(object sender, RoutedEventArgs e)
        {
            UIToItem();
            Item.OnUpdate(Item);
        }
    }
}
