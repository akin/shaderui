﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShaderUI.UI
{
    /// <summary>
    /// Interaction logic for ShaderList.xaml
    /// </summary>
    public partial class ShaderList : UserControl
    {
        public string Header
        {
            get
            {
                if (header == null)
                { 
                    return string.Empty;
                }
                return header.Content.ToString();
            }
            set
            {
                header.Content = value;
            }
        }

        private List<Shader> Items = new List<Shader>();
        public Action<Shader> OnEdit;

        public ShaderList()
        {
            InitializeComponent();
        }
        public void Set(List<Shader> items)
        {
            Items = items;
            foreach(var item in Items)
            {
                item.OnUpdate += Updated;
            }
            data.ItemsSource = Items;
        }
        public List<Shader> Get()
        {
            return Items;
        }
        public void Remove(Shader item)
        {
            item.OnUpdate -= Updated;
            if (Items != null)
            {
                Items.Remove(item);
                data.ItemsSource = null;
                data.ItemsSource = Items;
            }
        }
        public void Add(Shader item)
        {
            item.OnUpdate += Updated;
            if (Items != null)
            {
                Items.Add(item);
                data.ItemsSource = null;
                data.ItemsSource = Items;
            }
        }
        public void Edit(Shader item)
        {
            if(OnEdit != null)
            { 
                OnEdit(item); 
            }
        }
        public void Updated(Shader item)
        {
            data.ItemsSource = null;
            data.ItemsSource = Items;
        }
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow? row = sender as DataGridRow;
            if (row == null)
            {
                return;
            }

            Shader? item = row.DataContext as Shader;
            if (item == null)
            {
                return;
            }

            Edit(item);

            e.Handled = true;
        }
        public static T? FindParentOfType<T>(DependencyObject obj)
        where T : DependencyObject
        {
            while(obj != null)
            {
                if(obj is T)
                {
                    return obj as T;
                }
                obj = VisualTreeHelper.GetParent(obj);
            }
            return null;
        }
        private void MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                var result = VisualTreeHelper.HitTest(data, e.GetPosition(data));
                DataGridRow? row = FindParentOfType<DataGridRow>(result.VisualHit);

                ContextMenu? cm = null;
                if (row != null)
                {
                    cm = FindResource("contextMenuOnItem") as ContextMenu;
                    cm.PlacementTarget = row;
                    cm.DataContext = row;
                }
                else
                {
                    cm = FindResource("contextMenuNew") as ContextMenu;
                    cm.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
                    cm.DataContext = null;
                }

                cm.IsOpen = true;
                e.Handled = true;
            }
        }
        public void RequestEdit(object sender, RoutedEventArgs args)
        {
            MenuItem? menuItem = sender as MenuItem;
            if (menuItem == null)
            {
                return;
            }
            DataGridRow? row = menuItem.DataContext as DataGridRow;
            if(row == null)
            {
                return;
            }
            Shader item = row.DataContext as Shader;
            if (item == null)
            {
                return;
            }
            Edit(item);
        }
        public void RequestDelete(object sender, RoutedEventArgs args)
        {
            MenuItem? menuItem = sender as MenuItem;
            if (menuItem == null)
            {
                return;
            }
            DataGridRow? row = menuItem.DataContext as DataGridRow;
            if (row == null)
            {
                return;
            }
            Shader item = row.DataContext as Shader;
            if (item == null)
            {
                return;
            }

            var messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Remove(item);
            }
        }
        public void RequestNew(object sender, RoutedEventArgs args)
        {
            Shader item = new Shader();
            Add(item);
            Edit(item);
        }
    }
}
