﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShaderUI.UI
{
    /// <summary>
    /// Interaction logic for NodeList.xaml
    /// </summary>
    public partial class NodeList : UserControl
    {
        public string Header
        {
            get
            {
                if (header == null)
                { 
                    return string.Empty;
                }
                return header.Content.ToString();
            }
            set
            {
                header.Content = value;
            }
        }

        private List<Node> Nodes = new List<Node>();

        public NodeList()
        {
            InitializeComponent();
        }
        public void Set(List<Node> nodes)
        {
            Nodes = nodes;
            foreach(var node in Nodes)
            {
                node.OnUpdate += Updated;
            }
            data.ItemsSource = Nodes;
        }
        public List<Node> Get()
        {
            return Nodes;
        }
        public void Remove(Node node)
        {
            node.OnUpdate -= Updated;
            if (Nodes != null)
            {
                Nodes.Remove(node);
                data.ItemsSource = null;
                data.ItemsSource = Nodes;
            }
        }
        public void Add(Node node)
        {
            node.OnUpdate += Updated;
            if (Nodes != null)
            {
                Nodes.Add(node);
                data.ItemsSource = null;
                data.ItemsSource = Nodes;
            }
        }
        public void Edit(Node node)
        {
            var editor = new UI.NodeEditor(node);
            editor.Owner = Window.GetWindow(this);
            editor.ShowDialog();
        }
        public void Updated(Node node)
        {
            data.ItemsSource = null;
            data.ItemsSource = Nodes;
        }
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow? row = sender as DataGridRow;
            if (row == null) return;

            Node? node = row.DataContext as Node;
            if (node == null) return;

            Edit(node);

            e.Handled = true;
        }
        public static T? FindParentOfType<T>(DependencyObject obj)
        where T : DependencyObject
        {
            while(obj != null)
            {
                if(obj is T)
                {
                    return obj as T;
                }
                obj = VisualTreeHelper.GetParent(obj);
            }
            return null;
        }
        private void MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                var result = VisualTreeHelper.HitTest(data, e.GetPosition(data));
                DataGridRow? row = FindParentOfType<DataGridRow>(result.VisualHit);

                ContextMenu? cm = null;
                if (row != null)
                {
                    cm = FindResource("contextMenuOnItem") as ContextMenu;
                    cm.PlacementTarget = row;
                    cm.DataContext = row;
                }
                else
                {
                    cm = FindResource("contextMenuNew") as ContextMenu;
                    cm.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
                    cm.DataContext = null;
                }

                cm.IsOpen = true;
                e.Handled = true;
            }
        }
        public void RequestEdit(object sender, RoutedEventArgs args)
        {
            MenuItem? menuItem = sender as MenuItem;
            if (menuItem == null)
            {
                return;
            }
            DataGridRow? row = menuItem.DataContext as DataGridRow;
            if(row == null)
            {
                return;
            }
            Node node = row.DataContext as Node;
            if (node == null)
            {
                return;
            }
            Edit(node);
        }
        public void RequestDelete(object sender, RoutedEventArgs args)
        {
            MenuItem? menuItem = sender as MenuItem;
            if (menuItem == null)
            {
                return;
            }
            DataGridRow? row = menuItem.DataContext as DataGridRow;
            if (row == null)
            {
                return;
            }
            Node node = row.DataContext as Node;
            if (node == null)
            {
                return;
            }

            var messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Remove(node);
            }
        }
        public void RequestNew(object sender, RoutedEventArgs args)
        {
            Node node = new Node();
            Add(node);
            Edit(node);
        }
    }
}
