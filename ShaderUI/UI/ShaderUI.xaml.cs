﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json;

namespace ShaderUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string CurrentWorkspace = "";

        public MainWindow()
        {
            InitializeComponent();
            shaderData.OnEdit += RequestEditShader;
        }
        private string GetWorkspacePath(string path, string type)
        {
            return System.IO.Path.Combine(path, type);
        }
        public void CreateWorkspace(string path)
        {
            // path
            //  - node
            //   - json stuctures
            //  - shader
            //   - json stuctures
            // NEW!
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var nodePath = GetWorkspacePath(path, "node");
            var shaderPath = GetWorkspacePath(path, "shader");
            if (!Directory.Exists(nodePath))
            {
                Directory.CreateDirectory(nodePath);
            }
            if (!Directory.Exists(shaderPath))
            {
                Directory.CreateDirectory(shaderPath);
            }
        }
        public void OpenWorkspace(string path)
        {
            CurrentWorkspace = path;
            CreateWorkspace(CurrentWorkspace);
            RefreshWorkspace();
        }
        public void RefreshWorkspace()
        {
            // Load Node data
            {
                var path = GetWorkspacePath(CurrentWorkspace, "node");
                var files = Directory.EnumerateFiles(path, "*.json", SearchOption.AllDirectories);

                var items = new List<Node>();
                foreach (var file in files)
                {
                    try
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        using (StreamReader sr = new StreamReader(file))
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            var item = serializer.Deserialize<Node>(reader);
                            if(item != null)
                            {
                                items.Add(item);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                nodeData.Set(items);
            }

            // Load Shader data
            {
                var path = GetWorkspacePath(CurrentWorkspace, "shader");
                var files = Directory.EnumerateFiles(path, "*.json", SearchOption.AllDirectories);

                var items = new List<Shader>();
                foreach (var file in files)
                {
                    try
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        using (StreamReader sr = new StreamReader(file))
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            var item = serializer.Deserialize<Shader>(reader);
                            if (item != null)
                            {
                                items.Add(item);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                shaderData.Set(items);
            }
        }    
        public void SaveWorkspace()
        {
            // Go through all nodes in NodeList
            {
                var path = GetWorkspacePath(CurrentWorkspace, "node");
                List<string> oldFilesList = new List<string>();
                {
                    var files = Directory.EnumerateFiles(path, "*.json", SearchOption.AllDirectories);
                    oldFilesList = files.ToList();
                }

                List<string> newFiles = new List<string>();
                var items = nodeData.Get();
                foreach (var item in items)
                {
                    if(item == null)
                    {
                        continue;
                    }
                    string newPath = System.IO.Path.Combine(path, item.Name + ".json");
                    newFiles.Add(newPath);

                    JsonSerializer serializer = new JsonSerializer();
                    using (StreamWriter sw = new StreamWriter(newPath))
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        serializer.Serialize(writer, item);
                    }
                }

                var depricated = oldFilesList.Except(newFiles);
                foreach (var item in depricated)
                {
                    System.IO.File.Delete(item);
                }
            }

            // Go through all shaders in Shader
            {
                var path = GetWorkspacePath(CurrentWorkspace, "shader");
                List<string> oldFilesList = new List<string>();
                {
                    var files = Directory.EnumerateFiles(path, "*.json", SearchOption.AllDirectories);
                    oldFilesList = files.ToList();
                }

                List<string> newFiles = new List<string>();
                var items = shaderData.Get();
                foreach (var item in items)
                {
                    if (item == null)
                    {
                        continue;
                    }
                    string newPath = System.IO.Path.Combine(path, item.Name + ".json");
                    newFiles.Add(newPath);

                    JsonSerializer serializer = new JsonSerializer();
                    using (StreamWriter sw = new StreamWriter(newPath))
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        serializer.Serialize(writer, item);
                    }
                }

                var depricated = oldFilesList.Except(newFiles);
                foreach (var item in depricated)
                {
                    System.IO.File.Delete(item);
                }
            }
        }
        public void RequestSaveWorkspace(object sender, RoutedEventArgs args)
        {
            SaveWorkspace();
        }
        public void RequestOpenWorkspace(object sender, RoutedEventArgs args)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                
                if (result != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
                OpenWorkspace(dialog.SelectedPath);
            }
        }
        public void RequestExit(object sender, RoutedEventArgs args)
        {
            this.Close();
        }
        public void RequestEditShader(Shader item)
        {
            shaderEditor.Set(item);
        }
    }
}
