﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShaderUI
{
    public class Node : ICloneable
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; } = "";
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = "";
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; } = "";
        [JsonProperty(PropertyName = "connection")]
        public List<Port> Connection { get; set; } = new List<Port>();
        [JsonProperty(PropertyName = "shard")]
        public List<NodeShard> Shard { get; set; } = new List<NodeShard>();
        public Action<Node> OnUpdate;

        public object Clone()
        {
            var item = (Node)MemberwiseClone();
            item.Name = (string)Name.Clone();
            item.Type = (string)Type.Clone();
            item.Description = (string)Description.Clone();
            item.Connection = Connection.Select(item => (Port)item.Clone()).ToList();
            item.Shard = Shard.Select(item => (NodeShard)item.Clone()).ToList();
            return item;
        }
        public void Set(Node other)
        {
            Name = other.Name;
            Type = other.Type;
            Description = other.Description;
            Connection = other.Connection;
            Shard = other.Shard;
        }
        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            var other = (Node)obj;

            if (Name != other.Name || Type != other.Type || Description != other.Description)
            {
                return false;
            }
            
            if(Connection.Count != other.Connection.Count)
            {
                return false;
            }
            foreach(var item in Connection)
            {
                if(other.Connection.Any(other => other.Equals(item)))
                {
                    continue;
                }
                return false;
            }

            if (Shard.Count != other.Shard.Count)
            {
                return false;
            }
            foreach (var item in Shard)
            {
                if (other.Shard.Any(other => other.Equals(item)))
                {
                    continue;
                }
                return false;
            }

            return true;
        }
    }
}
